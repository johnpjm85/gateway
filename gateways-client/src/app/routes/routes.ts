import {Routes} from "@angular/router";

export const routes: Routes = [
  {path: '', redirectTo: 'gateways', pathMatch: 'full'},
  {path: 'gateways', loadChildren: () => import('../modules/gateway/gateway.module').then(m => m.GatewayModule)}
];
