import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Gateway} from "../models/gateway";
import {Observable} from "rxjs";
import {GatewayService} from "./gateway.service";

@Injectable({
  providedIn: 'root'
})
export class GatewayResolverService implements Resolve<Gateway> {

  constructor(private gatewayService: GatewayService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Gateway> | Promise<Gateway> | Gateway {
    let id = route.paramMap.get('id');
    return this.gatewayService.find(id);
  }


}
