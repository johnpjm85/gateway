import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {Observable} from "rxjs";
import {Gateway} from "../models/gateway";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class GatewayService {

  constructor(private http: HttpClient) { }

  get url(): string {
    return `${environment.apiUrl}/gateways`;
  }

  findAll(): Observable<Array<Gateway>> {
    return this.http.get<Array<Gateway>>(this.url);
  }

  find(id: string): Observable<Gateway> {
    const url = `${this.url}/${id}`;
    return this.http.get<Gateway>(url);
  }

  save(gateway: Gateway): Observable<Gateway> {
    return this.http.post<Gateway>(this.url, gateway);
  }
}
