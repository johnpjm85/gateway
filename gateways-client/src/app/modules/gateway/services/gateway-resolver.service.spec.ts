import { TestBed } from '@angular/core/testing';

import { GatewayResolverService } from './gateway-resolver.service';

describe('GatewayResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GatewayResolverService = TestBed.get(GatewayResolverService);
    expect(service).toBeTruthy();
  });
});
