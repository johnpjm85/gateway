import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GatewayRoutingModule} from "./routes/gateway-routing.module";
import {GatewayListComponent} from './components/gateway-list/gateway-list.component';
import {MaterialModule} from "../material/material.module";
import {GatewayNewDialogComponent} from './components/gateway-new-dialog/gateway-new-dialog.component';
import { GatewayDetailsComponent } from './components/gateway-details/gateway-details.component';
import {DeviceModule} from "../device/device.module";
import { GatewayFullMessageDialogComponent } from './components/gateway-full-message-dialog/gateway-full-message-dialog.component';


@NgModule({
  declarations: [
    GatewayListComponent,
    GatewayNewDialogComponent,
    GatewayDetailsComponent,
    GatewayFullMessageDialogComponent,
  ],
  imports: [
    CommonModule,
    GatewayRoutingModule,
    MaterialModule,
    DeviceModule,
  ],
  entryComponents: [
    GatewayNewDialogComponent,
    GatewayFullMessageDialogComponent
  ],
})
export class GatewayModule {
}
