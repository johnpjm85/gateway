import {Device} from "../../device/models/device";

export interface Gateway {
  id: string;
  name: string;
  ipAddress: string;
  devices: Array<Device>
}
