import {GatewayListComponent} from "../components/gateway-list/gateway-list.component";
import {Routes} from "@angular/router";
import {GatewayDetailsComponent} from "../components/gateway-details/gateway-details.component";
import {GatewayResolverService} from "../services/gateway-resolver.service";

export const routes: Routes = [
  { path: '', component: GatewayListComponent },
  {
    path: ':id',
    resolve: { gateway: GatewayResolverService },
    component: GatewayDetailsComponent
  },
];
