import {Component, OnInit} from '@angular/core';
import {Gateway} from "../../models/gateway";
import {ActivatedRoute} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {DeviceNewDialogComponent} from "../../../device/components/device-new-dialog/device-new-dialog.component";
import {Device} from "../../../device/models/device";
import {DeviceService} from "../../../device/service/device.service";
import {StatusChangedEvent} from "../../../device/models/statusChangedEvent";
import {DeviceRemoveConfirmDialogComponent} from "../../../device/components/device-remove-confirm-dialog/device-remove-confirm-dialog.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {GatewayFullMessageDialogComponent} from "../gateway-full-message-dialog/gateway-full-message-dialog.component";

@Component({
  selector: 'ms-gateway-details',
  templateUrl: './gateway-details.component.html',
  styleUrls: ['./gateway-details.component.scss']
})
export class GatewayDetailsComponent implements OnInit {

  gateway: Gateway;
  devicesCountLimit = 10;

  constructor(private route: ActivatedRoute,
              private dialog: MatDialog,
              private deviceService: DeviceService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.route.data
      .subscribe((data: { gateway: Gateway }) => {
        this.gateway = data.gateway;
      });
  }

  get isGatewayFullOfDevices(): boolean {
    return this.gateway.devices.length >= this.devicesCountLimit;
  }

  addNewDevice(): void {
    if (this.isGatewayFullOfDevices) {
      this.showFullGatewayMessage();
    } else {
      this.showNewDeviceDialog();
    }
  }

  deleteDevice(device: Device) {
    const dialogRef = this.dialog.open(DeviceRemoveConfirmDialogComponent, {
      width: '350px',
      data: { device: device }
    });

    dialogRef.afterClosed().subscribe((remove: boolean) => {
      if (remove) {
        this.deviceService.delete(this.gateway.id, device).subscribe(() => {
          const deviceIndex = this.gateway.devices.indexOf(device);
          this.gateway.devices.splice(deviceIndex, 1);
          this.snackBar.open('Device removed!', '', {
            duration: 2000
          });
        });
      }
    });
  }

  updateDeviceStatus($event: StatusChangedEvent) {
    this.deviceService.update(this.gateway.id, $event.device).subscribe(() => {});
  }

  editDevice($event: Device) {
    this.snackBar.open('Feature not implemented!', '', {
      duration: 2000
    });
  }

  private showFullGatewayMessage() {
    const dialogRef = this.dialog.open(GatewayFullMessageDialogComponent, {
      width: '350px',
    });
  }

  private showNewDeviceDialog() {
    const dialogRef = this.dialog.open(DeviceNewDialogComponent, {
      width: '350px',
      data: { gatewayId: this.gateway.id }
    });

    dialogRef.afterClosed().subscribe(device => {
      if (device != null) {
        this.gateway.devices.push(device);
        this.snackBar.open('Device added successfully!', '', {
          duration: 2000
        });
      }
    });
  }
}
