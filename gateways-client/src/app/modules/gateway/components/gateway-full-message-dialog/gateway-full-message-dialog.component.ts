import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'ms-gateway-full-message-dialog',
  templateUrl: './gateway-full-message-dialog.component.html',
  styleUrls: ['./gateway-full-message-dialog.component.scss']
})
export class GatewayFullMessageDialogComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<GatewayFullMessageDialogComponent>) { }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }
}
