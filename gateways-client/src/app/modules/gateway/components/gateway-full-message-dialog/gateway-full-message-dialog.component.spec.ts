import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GatewayFullMessageDialogComponent } from './gateway-full-message-dialog.component';

describe('GatewayFullMessageDialogComponent', () => {
  let component: GatewayFullMessageDialogComponent;
  let fixture: ComponentFixture<GatewayFullMessageDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GatewayFullMessageDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GatewayFullMessageDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
