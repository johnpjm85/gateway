import { Component, OnInit } from '@angular/core';
import {Gateway} from "../../models/gateway";
import {GatewayService} from "../../services/gateway.service";
import {MatDialog} from "@angular/material/dialog";
import {GatewayNewDialogComponent} from "../gateway-new-dialog/gateway-new-dialog.component";

@Component({
  selector: 'ms-gateway-list',
  templateUrl: './gateway-list.component.html',
  styleUrls: ['./gateway-list.component.scss']
})
export class GatewayListComponent implements OnInit {

  isLoading = true;
  gateways: Array<Gateway> = [];

  constructor(private gatewayService: GatewayService, private dialog: MatDialog) { }

  ngOnInit() {
    this.gatewayService.findAll().subscribe((gateways) => {
      this.gateways = gateways;
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
    });
  }

  addNew(): void {
    const dialogRef = this.dialog.open(GatewayNewDialogComponent, {
      width: '350px',
    });

    dialogRef.afterClosed().subscribe(gateway => {
      if (gateway != null) {
        this.gateways.push(gateway);
      }
    });
  }

}
