import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {Gateway} from "../../models/gateway";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {GatewayService} from "../../services/gateway.service";

@Component({
  selector: 'ms-gateway-new-dialog',
  templateUrl: './gateway-new-dialog.component.html',
  styleUrls: ['./gateway-new-dialog.component.scss']
})
export class GatewayNewDialogComponent implements OnInit {

  isSaving = false;
  ipv4Pattern = '^(?:(?:^|\\.)(?:2(?:5[0-5]|[0-4]\\d)|1?\\d?\\d)){4}$';

  gateway = new FormGroup({
    name: new FormControl(null, [Validators.required]),
    ipAddress: new FormControl(null, [Validators.required, Validators.pattern(this.ipv4Pattern)])
  });

  constructor(private dialogRef: MatDialogRef<GatewayNewDialogComponent>, private gatewayService: GatewayService) { }

  ngOnInit() {
  }

  get name(): AbstractControl {
    return this.gateway.get('name');
  }

  get ipAddress(): AbstractControl {
    return this.gateway.get('ipAddress');
  }

  cancel(): void {
    this.dialogRef.close();
  }

  save(): void {
    this.gateway.markAllAsTouched();
    if (this.gateway.invalid) {
      return;
    }

    this.isSaving = true;
    this.gatewayService.save(this.gateway.value).subscribe((gateway) => this.dialogRef.close(gateway));
  }
}
