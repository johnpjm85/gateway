import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GatewayNewDialogComponent } from './gateway-new-dialog.component';

describe('GatewayNewDialogComponent', () => {
  let component: GatewayNewDialogComponent;
  let fixture: ComponentFixture<GatewayNewDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GatewayNewDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GatewayNewDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
