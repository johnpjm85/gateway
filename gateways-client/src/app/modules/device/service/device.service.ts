import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Device} from "../models/device";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor(private http: HttpClient) { }

  save(gatewayId: string, device: Device): Observable<Device> {
    const url = this.getUrl(gatewayId);
    return this.http.post<Device>(url, device);
  }

  update(gatewayId: string, device: Device): Observable<Device> {
    const url = this.getDeviceUrl(gatewayId, device.id);
    return this.http.put<Device>(url, device);
  }

  delete(gatewayId: string, device: Device): Observable<Device> {
    const url = this.getDeviceUrl(gatewayId, device.id);
    return this.http.delete<Device>(url);
  }

  /**
   * Returns the resource url based on the gateway
   * @param gatewayId the identifier of the gateway
   */
  private getUrl(gatewayId: string): string {
    return `${environment.apiUrl}/gateways/${gatewayId}/devices`;
  }

  /**
   * Returns the resource url based on the gateway
   * @param gatewayId the identifier of the gateway
   * @param deviceId the identifier of the device
   */
  private getDeviceUrl(gatewayId: string, deviceId: number): string {
    return `${this.getUrl(gatewayId)}/${deviceId}`;
  }
}
