import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeviceListComponent } from './components/device-list/device-list.component';
import {MaterialModule} from "../material/material.module";
import { DeviceNewDialogComponent } from './components/device-new-dialog/device-new-dialog.component';
import { DeviceRemoveConfirmDialogComponent } from './components/device-remove-confirm-dialog/device-remove-confirm-dialog.component';



@NgModule({
  declarations: [
    DeviceListComponent,
    DeviceNewDialogComponent,
    DeviceRemoveConfirmDialogComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    DeviceListComponent,
    DeviceNewDialogComponent,
  ],
  entryComponents: [
    DeviceNewDialogComponent,
    DeviceRemoveConfirmDialogComponent
  ]
})
export class DeviceModule { }
