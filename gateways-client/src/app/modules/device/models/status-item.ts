import {StatusEnum} from "./status-enum";

export interface StatusItem {
  label: string,
  value: StatusEnum
}
