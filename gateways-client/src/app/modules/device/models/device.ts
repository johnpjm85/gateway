import {StatusEnum} from "./status-enum";

export class Device {
  id: number;
  vendor: string;
  dateCreated: Date;
  status: StatusEnum;
}
