import {StatusEnum} from "./status-enum";
import {Device} from "./device";

export interface StatusChangedEvent {
  device: Device;
  status: StatusEnum;
  prevStatus: StatusEnum;
}
