import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Device} from "../../models/device";

@Component({
  selector: 'app-device-remove-confirm-dialog',
  templateUrl: './device-remove-confirm-dialog.component.html',
  styleUrls: ['./device-remove-confirm-dialog.component.scss']
})
export class DeviceRemoveConfirmDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: ({device: Device}),
              private dialogRef: MatDialogRef<DeviceRemoveConfirmDialogComponent>) { }

  ngOnInit() {
  }

  ok() {
    this.dialogRef.close(true);
  }

  cancel() {
    this.dialogRef.close(false);
  }
}
