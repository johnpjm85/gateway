import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceRemoveConfirmDialogComponent } from './device-remove-confirm-dialog.component';

describe('DeviceRemoveConfirmDialogComponent', () => {
  let component: DeviceRemoveConfirmDialogComponent;
  let fixture: ComponentFixture<DeviceRemoveConfirmDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceRemoveConfirmDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceRemoveConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
