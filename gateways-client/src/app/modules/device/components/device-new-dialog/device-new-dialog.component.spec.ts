import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceNewDialogComponent } from './device-new-dialog.component';

describe('DeviceNewDialogComponent', () => {
  let component: DeviceNewDialogComponent;
  let fixture: ComponentFixture<DeviceNewDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceNewDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceNewDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
