import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {DeviceService} from "../../service/device.service";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {StatusItem} from "../../models/status-item";
import {StatusEnum} from "../../models/status-enum";

@Component({
  selector: 'ms-device-new-dialog',
  templateUrl: './device-new-dialog.component.html',
  styleUrls: ['./device-new-dialog.component.scss']
})
export class DeviceNewDialogComponent implements OnInit {

  isSaving = false;

  statusItems: Array<StatusItem> = [
    { label: 'On', value: StatusEnum.ONLINE },
    { label: 'Off', value: StatusEnum.OFFLINE },
  ];

  device = new FormGroup({
    vendor: new FormControl(null, [Validators.required]),
    status: new FormControl(StatusEnum.OFFLINE, [Validators.required]),
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: ({gatewayId: string}),
              private dialogRef: MatDialogRef<DeviceNewDialogComponent>,
              private deviceService: DeviceService) {
  }

  ngOnInit() {
  }

  get vendor(): AbstractControl {
    return this.device.get('vendor');
  }

  cancel(): void {
    this.dialogRef.close();
  }

  save(): void {
    this.device.markAllAsTouched();
    if (this.device.invalid) {
      return;
    }

    this.isSaving = true;
    this.deviceService.save(this.data.gatewayId, this.device.value).subscribe((device) => {
      this.dialogRef.close(device);
    });
  }
}
