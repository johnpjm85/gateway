import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Device} from "../../models/device";
import {MatSlideToggleChange} from "@angular/material/slide-toggle";
import {StatusEnum} from "../../models/status-enum";
import {StatusChangedEvent} from "../../models/statusChangedEvent";

@Component({
  selector: 'ms-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.scss']
})
export class DeviceListComponent implements OnInit {

  @Input() devices: Array<Device>;
  @Output() statusChanged = new EventEmitter<StatusChangedEvent>();
  @Output() deleteDevice = new EventEmitter<Device>();
  @Output() editDevice = new EventEmitter<Device>();

  constructor() { }

  ngOnInit() {
  }

  notifyItemChangeStatus($event: MatSlideToggleChange, device: Device): void {
    device.status = $event.checked ? StatusEnum.ONLINE : StatusEnum.OFFLINE;
    this.statusChanged.emit({
      device: device,
      status: device.status,
      prevStatus: $event.checked ? StatusEnum.OFFLINE : StatusEnum.ONLINE
    });
  }

  notifyItemDelete(device: Device): void {
    this.deleteDevice.emit(device);
  }

  notifyItemEdit(device: Device) {
    this.editDevice.emit(device);
  }
}
