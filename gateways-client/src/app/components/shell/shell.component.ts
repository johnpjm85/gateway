import { Component } from '@angular/core';

@Component({
  selector: 'ms-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent {
  title = 'Gateways Client';
}
