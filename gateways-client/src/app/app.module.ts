import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {ShellComponent} from "./components/shell/shell.component";
import {AppRoutingModule} from "./routes/app-routing.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    ShellComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [ShellComponent]
})
export class AppModule { }
