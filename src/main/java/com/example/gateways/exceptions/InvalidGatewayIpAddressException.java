package com.example.gateways.exceptions;

/**
 * This exception is thrown when trying to save a Gateway with an invalid IP v4 address
 */
public class InvalidGatewayIpAddressException extends GatewayAppException {
    public InvalidGatewayIpAddressException(String ipAddress) {
        super("IP address is not a valid Ipv4 address. Ip address: " + ipAddress);
    }
}
