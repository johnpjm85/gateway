package com.example.gateways.exceptions;

public class GatewayDevicesLimitException extends GatewayAppException {
    public GatewayDevicesLimitException(String gatewayId) {
        super(String.format("Gateway '%s' reached it's devices limit. Gateways can't handle more than 10 devices.", gatewayId));
    }
}
