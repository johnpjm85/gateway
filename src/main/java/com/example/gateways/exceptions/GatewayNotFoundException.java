package com.example.gateways.exceptions;

/**
 * This exception is thrown when the requested Gateway was not found.
 */
public class GatewayNotFoundException extends GatewayAppException {
    public GatewayNotFoundException(String id) {
        super(String.format("Gateway with id: '%s' not found.", id));
    }
}
