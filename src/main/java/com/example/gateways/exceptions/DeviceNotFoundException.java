package com.example.gateways.exceptions;

/**
 * This exception is thrown when the requested device was not found.
 */
public class DeviceNotFoundException extends GatewayAppException {
    public DeviceNotFoundException(String gatewayId, int deviceId) {
        super(String.format("Device '%d' not found in gateway '%s'.", deviceId, gatewayId));
    }
}
