package com.example.gateways.exceptions;

/**
 * This is the base class for all application exceptions
 */
public class GatewayAppException extends RuntimeException {
    public GatewayAppException(String message) {
        super(message);
    }
}
