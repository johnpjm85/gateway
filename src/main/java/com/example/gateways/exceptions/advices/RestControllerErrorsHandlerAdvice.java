package com.example.gateways.exceptions.advices;

import com.example.gateways.exceptions.DeviceNotFoundException;
import com.example.gateways.exceptions.GatewayDevicesLimitException;
import com.example.gateways.exceptions.GatewayNotFoundException;
import com.example.gateways.exceptions.InvalidGatewayIpAddressException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestControllerAdvice
public class RestControllerErrorsHandlerAdvice {
    @ExceptionHandler({GatewayNotFoundException.class})
    public void handleGatewayNotFoundException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value());
    }

    @ExceptionHandler({InvalidGatewayIpAddressException.class})
    public void handleInvalidGatewayIpAddressException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler({GatewayDevicesLimitException.class})
    public void handleGatewayDevicesLimitException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler({DeviceNotFoundException.class})
    public void handleDeviceNotFoundException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value());
    }
}
