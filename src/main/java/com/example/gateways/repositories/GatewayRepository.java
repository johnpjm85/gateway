package com.example.gateways.repositories;

import com.example.gateways.models.Gateway;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GatewayRepository extends MongoRepository<Gateway, String> {
}
