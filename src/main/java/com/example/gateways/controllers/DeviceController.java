package com.example.gateways.controllers;

import com.example.gateways.exceptions.GatewayDevicesLimitException;
import com.example.gateways.models.Device;
import com.example.gateways.models.Gateway;
import com.example.gateways.services.GatewayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/gateways/{gatewayId}/devices")
public class DeviceController {
    private GatewayService gatewayService;

    @Autowired
    DeviceController(GatewayService gatewayService) {
        this.gatewayService = gatewayService;
    }

    @PostMapping
    public ResponseEntity<Device> save(@PathVariable("gatewayId") String gatewayId, @RequestBody Device device, UriComponentsBuilder uriComponentsBuilder) {
        Gateway gateway = this.gatewayService.findById(gatewayId);

        if (gateway.getDevices().size() == 10) {
            throw new GatewayDevicesLimitException(gatewayId);
        }

        device.setId(UUID.randomUUID().hashCode());
        device.setDateCreated(new Date());

        gateway.getDevices().add(device);
        this.gatewayService.save(gateway);

        URI location = uriComponentsBuilder
                .path("/gateways/")
                .path(gatewayId)
                .path("/devices/")
                .path(String.valueOf(device.getId()))
                .build()
                .toUri();

        return ResponseEntity
                .created(location)
                .body(device);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Device delete(@PathVariable("gatewayId") String gatewayId, @PathVariable("id") int id) {
        Gateway gateway = this.gatewayService.findById(gatewayId);
        Device device = gateway.getDeviceById(id);

        gateway.getDevices().remove(device);
        this.gatewayService.save(gateway);

        return device;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Device update(@PathVariable("gatewayId") String gatewayId, @PathVariable("id") int id, @RequestBody Device data) {
        Gateway gateway = this.gatewayService.findById(gatewayId);
        Device device = gateway.getDeviceById(id);

        device.updateFrom(data);
        this.gatewayService.save(gateway);

        return device;
    }
}
