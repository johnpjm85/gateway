package com.example.gateways.controllers;

import com.example.gateways.exceptions.InvalidGatewayIpAddressException;
import com.example.gateways.models.Gateway;
import com.example.gateways.services.GatewayService;
import com.example.gateways.validators.IpAddressValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/gateways")
public class GatewayController {
    private GatewayService gatewayService;

    @Autowired
    public GatewayController(GatewayService gatewayService) {
        this.gatewayService = gatewayService;
    }

    @GetMapping
    public List<Gateway> findAll() {
        return this.gatewayService.findAll();
    }

    @GetMapping("/{id}")
    public Gateway findById(@PathVariable("id") String id) {
        return this.gatewayService.findById(id);
    }

    @PostMapping
    public ResponseEntity<Gateway> save(@RequestBody Gateway gateway, UriComponentsBuilder uriComponentsBuilder) {
        if (!IpAddressValidator.validateIpv4(gateway.getIpAddress())) {
            throw new InvalidGatewayIpAddressException(gateway.getIpAddress());
        }

        this.gatewayService.save(gateway);

        URI location = uriComponentsBuilder
                .path("/gateways/")
                .path(gateway.getId())
                .build()
                .toUri();

        return ResponseEntity
                .created(location)
                .body(gateway);
    }
}
