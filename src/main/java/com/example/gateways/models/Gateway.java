package com.example.gateways.models;

import com.example.gateways.exceptions.DeviceNotFoundException;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Gateway {
    @Id
    private String id;
    private String name;
    private String ipAddress;
    private List<Device> devices = new ArrayList<>();

    public Gateway() {}

    public Gateway(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * Returns a device given its ID
     * @param id the id of the device
     * @return an instance of the device
     * @throws DeviceNotFoundException when the device was not found
     */
    public Device getDeviceById(int id) throws DeviceNotFoundException {
        Optional<Device> deviceToRemove = this.getDevices().stream()
                .filter(device -> device.getId() == id)
                .findFirst();

        if (!deviceToRemove.isPresent()) {
            throw new DeviceNotFoundException(this.getId(), id);
        }

        return deviceToRemove.get();
    }
}
