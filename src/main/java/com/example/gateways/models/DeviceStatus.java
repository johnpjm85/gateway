package com.example.gateways.models;

public enum DeviceStatus {
    OFFLINE,
    ONLINE
}
