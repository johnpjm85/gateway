package com.example.gateways.models;

import java.util.Date;

public class Device {

    private int id;
    private String vendor;
    private Date dateCreated;
    private DeviceStatus status = DeviceStatus.OFFLINE;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public DeviceStatus getStatus() {
        return status;
    }

    public void setStatus(DeviceStatus status) {
        this.status = status;
    }

    public void updateFrom(Device other) {
        this.vendor = other.vendor;
        this.status = other.status;
    }
}
