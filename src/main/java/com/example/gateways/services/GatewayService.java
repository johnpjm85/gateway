package com.example.gateways.services;

import com.example.gateways.exceptions.GatewayNotFoundException;
import com.example.gateways.models.Gateway;
import com.example.gateways.repositories.GatewayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class GatewayService {
    private GatewayRepository gatewayRepository;

    @Autowired
    public GatewayService(GatewayRepository gatewayRepository) {
        this.gatewayRepository = gatewayRepository;
    }

    public List<Gateway> findAll() {
        return this.gatewayRepository.findAll();
    }

    public Gateway findById(String id) throws GatewayNotFoundException {
        Optional<Gateway> gateway = this.gatewayRepository.findById(id);
        if (!gateway.isPresent()) {
            throw new GatewayNotFoundException(id);
        }

        return gateway.get();
    }

    public Gateway save(Gateway gateway) {
        return this.gatewayRepository.save(gateway);
    }
}
