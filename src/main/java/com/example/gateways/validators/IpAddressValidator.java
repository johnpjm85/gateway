package com.example.gateways.validators;

public class IpAddressValidator {
    private final static String IPV4PATTERN = "^(?:(?:^|\\.)(?:2(?:5[0-5]|[0-4]\\d)|1?\\d?\\d)){4}$";

    public static boolean validateIpv4(String ipAddress) {
        return ipAddress != null && ipAddress.matches(IPV4PATTERN);
    }
}
