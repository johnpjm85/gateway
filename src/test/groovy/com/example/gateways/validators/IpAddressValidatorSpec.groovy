package com.example.gateways.validators

import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class IpAddressValidatorSpec extends Specification {
    def "null IP address is not valid"() {
        when: "null IP address is set"
        def ipAddress = null
        def isValid = IpAddressValidator.validateIpv4(ipAddress)

        then: "the address is not valid"
        !isValid
    }

    def "invalid IP v4 address"() {
        when: "invalid IP v4 address is set"
        def ipAddress = '10.256.255.1'
        def isValid = IpAddressValidator.validateIpv4(ipAddress)

        then: "the address is not valid"
        !isValid
    }

    def "valid IP v4 address"() {
        when: "valid IP v4 address is set"
        def ipAddress = '10.255.255.1'
        def isValid = IpAddressValidator.validateIpv4(ipAddress)

        then: "the address is valid"
        isValid
    }
}
