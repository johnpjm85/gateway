package com.example.gateways.controllers

import com.example.gateways.exceptions.GatewayDevicesLimitException
import com.example.gateways.exceptions.advices.RestControllerErrorsHandlerAdvice
import com.example.gateways.models.Device
import com.example.gateways.models.Gateway
import com.example.gateways.services.GatewayService
import groovy.json.JsonBuilder
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.util.UriComponentsBuilder
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
class DeviceControllerSpec extends Specification {

    def "add more than 10 devices to a gateway throws an exception"() {
        given: "a gateway with 10 devices"
        def gateway = createFullGateway()
        DeviceController deviceController = createDeviceController(gateway)

        when: "save a device into the gateway"
        deviceController.save(gateway.id, new Device(), UriComponentsBuilder.newInstance())

        then: "a GatewayDevicesLimitException is thrown"
        thrown GatewayDevicesLimitException
    }

    def "post more than 10 devices to a gateway returns a BAD_REQUEST HTTP status code"() {
        given: "a gateway with 10 devices"
        def gateway = createFullGateway()
        DeviceController deviceController = createDeviceController(gateway)
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(deviceController)
                .setControllerAdvice(new RestControllerErrorsHandlerAdvice())
                .build()

        when: "perform a post request for adding a device to the gateway"
        def requestBody = [ vendor: 'Lenovo', status: 'ONLINE' ]
        def request = post("/gateways/${gateway.id}/devices")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(requestBody))

        def results = mockMvc.perform(request)

        then: "a BAD_REQUEST HTTP status code is returned"
        results.andExpect(status().isBadRequest())
    }

    private static Gateway createFullGateway() {
        def gateway = new Gateway(UUID.randomUUID().toString())
        gateway.devices.addAll([
                new Device(),
                new Device(),
                new Device(),
                new Device(),
                new Device(),
                new Device(),
                new Device(),
                new Device(),
                new Device(),
                new Device(),
        ])
        gateway
    }

    private DeviceController createDeviceController(Gateway gateway) {
        GatewayService gatewayService = Stub {
            findById(gateway.id) >> gateway
        }

        def deviceController = new DeviceController(gatewayService)
        deviceController
    }

    private static String toJson(Map<String, Object> map) {
        return new JsonBuilder(map).toString()
    }
}
