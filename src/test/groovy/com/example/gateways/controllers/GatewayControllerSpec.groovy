package com.example.gateways.controllers

import com.example.gateways.exceptions.InvalidGatewayIpAddressException
import com.example.gateways.exceptions.advices.RestControllerErrorsHandlerAdvice
import com.example.gateways.models.Gateway
import groovy.json.JsonBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.util.UriComponentsBuilder
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
class GatewayControllerSpec  extends Specification {

    @Autowired
    private GatewayController gatewayController

    private MockMvc mockMvc

    def setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(gatewayController)
                .setControllerAdvice(new RestControllerErrorsHandlerAdvice())
                .build()
    }

    def "post a gateway with invalid ip address returns BAD_REQUEST HTTP status code"() {
        given: "a request body with a gateway with invalid ip v4 address"
        Map requestBody = [
                name     : 'Gateway 1',
                ipAddress: '10.255.256.1'
        ]

        def request = post("/gateways",)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(requestBody))

        when: "perform a post request for creating a Gateway"
        def results = mockMvc.perform(request)

        then: "BAD_REQUEST status code is returned due to the invalid ip address"
        results.andExpect(status().isBadRequest())
    }

    def "save a gateway with invalid ip address rises an exception"() {
        given: "a gateway with invalid ip v4 address"
        Gateway gateway = new Gateway()
        gateway.name = 'Gateway 1'
        gateway.ipAddress = '10.255.256.1'

        when: "save the gateway"
        gatewayController.save(gateway, UriComponentsBuilder.newInstance())

        then: "an InvalidGatewayIpAddressException exception is thrown"
        thrown InvalidGatewayIpAddressException
    }

    private static String toJson(Map<String, Object> map) {
        return new JsonBuilder(map).toString()
    }
}
