## "Gateway application"

### Intro

The project is composed by two projects:
* An Angular web application.
* REST services developed in Spring Boot.

The project also implements a connection to a Mongo DB as the storage repository.

### Project structure

The root folder hosts a Spring Boot application configured with Gradle. <br>
The second must important folder is ***gateways-client*** that holds the 
Angular application source code.

### Build & Deploy with Docker

You can run and deploy the project locally only with Docker. Nothing else is needed, not gradle, not nodejs, just Docker and *docker-compose*.
In the root folder run the command:

```shell script
docker-compose up
```
This command will deploy three docker containers:
* **gateway-web-app** (Nginx server hosting the Angular application through port **81**)
* **gateway-web-services** (A Java based container running the Spring Boot application and listening through port **82**)
* **mongodb** (The MongoDb server)

The file ***docker-compose.yml*** contains the configuration for deploy the containers.

After run the command line docker-compose open a browser in URL ***http://localhost:81***

The Angular app and the Spring Boot app are build and deploy within the containers using the multistage feature of Docker.
The docker files for each project are:
* Angular project in: ***/gateways-client/Dockerfile***
* Spring Boot project in: ***Dockerfile***

### Run tests

##### Prerequisites
Before you run test, make sure your development environment includes **Gradle** and **Java** >= 8.0

Gateway application contains some unit tests written with the **Spock** test framework. The tests are located
in folder "***/src/test/groovy/***". For executing tests run the commandline:
```shell script
./gradlew test
``` 
The test reports are located in folder "***/build/reports/tests/test/***". <br> 
The gradle also include a dependency to **spock-reports** a plugin for generating reports of **Spock** test. 
The reports are located in folder "***/build/spock-reports/***"

### Run spring services

You can build and run locally the Spring Boot project.

##### Prerequisites
1- Before you begin, make sure your development environment includes **Gradle** and **Java** >= 8.0 <br>
2- You also must have a **MongoDB** running locally in **localhost:27017**.

##### Step 1. Build and run application:
```shell script
gradlew bootRun
```
This will start the REST services listen through URL **http://localhost:8080**.
You can test listing the gateways with command:

```shell script
curl http://localhost:8080/rest/gateways
```

### Run web application 

You can also build and run the Angular app locally in a development server. 

##### Prerequisites

Before you begin, make sure your development environment includes Node.js and an npm package manager.

##### Step 1. Install the Angular CLI:
```shell script
npm install -g @angular/cli
```
##### Step 2. Move to Angular project folder:
```shell script
cd gateways-client
```
##### Step 3. Install project dependencies:
```shell script
npm install
```
##### Step 4. Run the application:
```shell script
ng serve --open
```
This will open the browser with the application opened in the URL ***http://localhost:4200***
The Angular app have configured the URL of the REST services in the ***environment*** configuration 
files located in folder ***gateways-client/src/environments***. The property **environment.apiUrl**
specifies the services URL. For example the production configuration file **environment.prod**
contains the value:
```typescript
export const environment = {
  production: true,
  apiUrl: 'http://localhost:82/rest'
};
```
